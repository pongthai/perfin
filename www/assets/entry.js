var app = angular.module('perfin');

app.controller('entry', function($rootScope, $scope, $http, $routeParams, $timeout, listAccounts) {
    $scope.alertMessage = { show: false, class: 'alert-success', message: '' };
    $scope.trans_id = null; // null = Create mode, not-null = edit mode

    $scope.addCatAlertMessage = {
        show: false,
        type: 'alert-success',
        content: ''
    };

    $scope.clearEntry = function() {
        $scope.entry = {};
        $scope.entry.account = "";
        $scope.entry.type = "";
        $scope.entry.payee = "";
        $scope.entry.category = "";
        $scope.entry.amount = "";
        $scope.entry.date = new Date().toISOString();
        if ($scope.dateChanged !== undefined) $scope.dateChanged();
    };

    $scope.clearEntry();

    if ($routeParams.a !== undefined) $scope.account = $routeParams.a;

    // If :trans_id is provided, enter an edit mode.
    if ($routeParams.trans_id !== undefined) {
        $scope.trans_id = $routeParams.trans_id;
        // Load the transaction to fill in the form.

        $http.get('/api/trans/' + $scope.trans_id)
            .then(ret => {
                if (ret.data.success) {
                    $scope.entry = {
                        account: ret.data.result.account,
                        type: ret.data.result.type,
                        category: ret.data.result.category,
                        date: ret.data.result.date,
                        payee: ret.data.result.payee,
                        toAccount: ret.data.result.toAccount,
                        note: ret.data.result.note,
                        amount: ret.data.result.amount
                    };

                    $scope.dateChanged();
                    $scope.catAutoComplete();
                } else {
                    $scope.alert('Failed to retrieve the transaction to edit.', 'alert-danger');
                    $scope.clearEntry();
                }
            });
    } else {
        // No trans_id provided, create mode.
        $scope.entry = {
            account: $scope.account,
            type: '',
            category: '',
            date: '',
            payee: '',
            toAccount: '',
            note: '',
            amount: 0
        };
    }

    $scope.accounts = [];

    $scope.formatDate = function(d) {
        return (d.getMonth() + 1) + '/' + d.getDate() + '/' + (d.getYear() + 1900);
    }

    $scope.formatTime = function(d) {
        return (d.getHours() <= 9 ? '0' : '') + (d.getHours()) + ':' + (d.getMinutes() <= 9 ? '0' : '') + d.getMinutes();
    }

    if ($scope.entry.date === '') {
        $scope.entry.date = new Date().toISOString();
    }


    $scope.txtDateChanged = function() {
        let dd = 0;
        let mm = 0;
        let yyyy = 0;
        let hh = 0;
        let min = 0;

        function pad(a, pos) {
            let l = pos - a.length;
            for (let i = 0; i < l; i++) a = 0 + a;
            return a;
        }

        var datecomp = /(\d+)\/(\d+)\/(\d{4})/.exec($scope.dateEntry.date);
        if (datecomp !== null) {
            mm = pad(datecomp[1], 2);
            dd = pad(datecomp[2], 2);
            yyyy = datecomp[3];
        }

        var timecomp = /(\d+):(\d+)/.exec($scope.dateEntry.time);

        if (timecomp !== null) {
            hh = pad(timecomp[1], 2);
            min = pad(timecomp[2], 2);
        }

        var txtdate = yyyy + '-' + mm + '-' + dd + "T" + hh + ':' + min + ':00.000+0700';

        if (!isNaN(Date.parse(txtdate))) {
            $scope.entry.date = txtdate;
            console.log("Date format good " + txtdate);
            $scope.dateChanged();
        } else {
            console.log("Date format error " + txtdate);
        }
    };

    $scope.dateChanged = function() {
        $scope.dateEntry = {
            date: $scope.formatDate(new Date($scope.entry.date)),
            time: $scope.formatTime(new Date($scope.entry.date))
        }
    };


    $scope.dateChanged();

    $scope.newcat = {
        _id: '',
        parent: '',
        type: 'EXPENSE',
        icon: 'glypicon glphicon-home',
        color: '#ffccff'
    };
    $scope.cats = null;
    $scope.catsFiltered = [];
    $scope.payeeList = [];

    listAccounts()
        .then(function(accounts) {
            $scope.accounts = accounts;
        });

    $scope.submitEntry = function() {
        if ($scope.trans_id === null) {
            // Create Mode
            $http.post('/api/accounts/' + $scope.entry.account + '/trans', $scope.entry)
                .then(function(result) {

                        if (result.data.success) {
                            $scope.alert('Transaction Added Successfully', 'alert-success');
                            $scope.clearEntry();
                        } else {
                            $scope.alert('Could not create the transaction', 'alert-danger');
                        }
                    },
                    function(err) {
                        console.log('Error:  ' + JSON.stringify(err));
                        if (err.data.error === undefined) { err.data.error = { msg: err.statusText } };
                        $scope.alert('Error: ' + err.data.error.msg, 'alert-danger');
                    });
        } else {
            // Edit Mode
            $http.put('/api/accounts/' + $scope.entry.account + '/trans/' + $scope.trans_id, $scope.entry)
                .then(function(result) {
                    if (result.data.success) {
                        $scope.alert('Modification Saved.', 'alert-success');
                        $scope.clearEntry();
                    } else {
                        $scope.alert('Failed to save the modifications.', 'alert-danger');
                        console.log('Failed to save modifications: ' + JSON.stringify(result.data));
                    }
                })
                .catch(function(err) {
                    console.log('Error: ' + JSON.stringify(err));
                    $scope.alert('Error: ' + err.data.error.msg, 'alert-danger');
                });
        }
    };


    // this function is for the next function
    function buildCatSlug(cat) {
        if (cat.parent !== undefined && cat.parent !== null) {
            var parentData;
            for (var i = 0; i < $scope.cats.length; i++) {
                if ($scope.cats[i]._id === cat.parent) {
                    parentData = $scope.cats[i];
                    break;
                }
            }
            return buildCatSlug(parentData) + ' > ' + cat._id;
        } else {
            return cat._id;
        }
    };

    $scope.loadCategories = function() {
        $http.get('/api/cats')
            .success(function(cats) {
                if (cats.success) {
                    $scope.cats = cats.result;
                    // build a slug
                    $scope.cats.forEach(function(cat) {
                        cat.slug = buildCatSlug(cat);
                    });
                    // Sort so that it is sorted by slug
                    $scope.cats.sort(function(a, b) {
                        if (a.slug < b.slug) return -1;
                        else if (a.slug > b.slug) return 1;
                        else return 0;
                    });
                    $scope.catsFiltered = $scope.cats;
                    $scope.catAutoComplete();
                }
            });
    }

    $scope.showCatChoices = function(bshow) {
        if (bshow) {
            $('#catChoices').show();
        } else {
            $('#catChoices').hide();
        }
    };

    $scope.catAutoComplete = function() {
        $scope.catsFiltered = $scope.cats.filter(function(c) {
            var re = new RegExp("^" + $scope.entry.category, 'i');
            return re.test(c._id);
        });

        // Limit to 5 results only --- Not valid now.
        // $scope.catsFiltered = $scope.catsFiltered.slice(0,5);
    }

    $scope.iconList = [
        "glyphicon glyphicon-home",
        "glyphicon glyphicon-heart",
        "glyphicon glyphicon-star",
        "glyphicon glyphicon-plane",
        "glyphicon glyphicon-briefcase",
        "glyphicon glyphicon-wrench",
        "glyphicon glyphicon-tree-conifer",
        "fa fa-cutlery",
        "fa fa-car",
        "fa fa-bus",
        "fa fa-building",
        "fa fa-coffee",
        "fa fa-diamond",
        "fa fa-film",
        "fa fa-institution",
        "fa fa-plug",
        "fa fa-shopping-cart",
        "fa fa-trash",
        "fa fa-truck",
        "fa fa-subway",
        "fa fa-bed",
        "fa fa-child",
        "fa fa-camera",
        "fa fa-puzzle-piece",
        "fa fa-soccer-ball-o",
        "fa fa-stethoscope",
        "fa fa-trophy",
        "fa fa-gift",
        "fa fa-birthday-cake",
        "fa fa-laptop",
        "fa fa-gamepad",
        "fa fa-phone",
        "fa fa-flash",
        "fa fa-tint",
        "fa fa-suitcase"
    ];

    $scope.colorList = [
        "#ffccff",
        "#ffccaa",
        "#ccffaa",
        "#ccccff",
        "#ffaacc",
        "#ff66aa",
        "#cffccf",
        "#aaccff",
        "#ccaaff"
    ];

    $scope.autocompletePayee = function(value) {
        return $http.get('/api/payees?l=5&q=' + value)
            .then(function(res) {
                if (res.data.success) {
                    return res.data.result;
                } else {
                    return [];
                }
            });
    }

    $scope.payeeSelected = function(item, model, label) {
        // $scope.accounts.forEach(function(acc) { if (acc._id===item.account) $scope.entry.account =  acc.title; });
        $scope.entry.account = item.account;
        $scope.entry.amount = item.amount;
        $scope.entry.category = item.category;
        $scope.entry.type = item.type;
        if ($scope.entry.type === 'TRANSFER') {
            $scope.entry.toAccount = item.toAccount;
        } else {
            $scope.entry.toAccount = null;
        }
    };

    $scope.payeeAutoComplete = function() {

        if ($scope.entry.payee.length > 2) {
            $http.get('/api/payees?l=5&q=' + $scope.entry.payee)
                .success(function(data) {
                    if (data.success) {
                        $scope.payeeList = data.result;
                        if ($scope.payeeList.length > 0) {
                            $('#payeeDropdown').dropdown('toggle');
                        } else {
                            console.log("<0");
                            $('#payeeDropdown').dropdown('toggle');
                        }
                    }


                });
        } else {

        }
    };

    $scope.alert = function(message, msgclass) {
        $scope.alertMessage.message = message;
        $scope.alertMessage.class = msgclass ? msgclass : "alert-success";
        $scope.alertMessage.show = true;
        $timeout(function() {
            $scope.alertMessage.show = false;
        }, 3000);
    };

    $scope.payeeSelect = function(payeeId) {
        $('#payeeDropdown').dropdown('toggle');
        $scope.entry.payee = payeeId;
    };

    // Run once to query all categories initially
    $scope.loadCategories();

    $scope.addCategory = function() {
        if ($scope.newcat.parent !== '') {
            if ($scope.newcat._id !== '') {
                if ($scope.newcat.parent === 'null') $scope.newcat.parent = null;

                $http.put('/api/cats/' + $scope.newcat._id, $scope.newcat)
                    .success(function() {
                        $('#addCatDlg').modal('hide');
                        $scope.loadCategories();

                        $scope.newcat._id = ""; // We do not clear other fields since they may be more ergonomic for creating similar cats in series
                    })
                    .error(function(err) {
                        $scope.addCatAlertMessage.type = 'alert-danger';
                        $scope.addCatAlertMessage.content = err.error.msg;
                        $scope.addCatAlertMessage.show = true;
                    });
            } else {
                $scope.addCatAlertMessage.type = 'alert-danger';
                $scope.addCatAlertMessage.content = 'Please give a non-blank name to the category';
                $scope.addCatAlertMessage.show = true;
            }
        } else {
            $scope.addCatAlertMessage.type = 'alert-danger';
            $scope.addCatAlertMessage.content = 'Please select a valid parent category (field "Under")';
            $scope.addCatAlertMessage.show = true;
        }
    };
});