var app = angular.module('perfin',['ngRoute','ui.bootstrap']);

app.controller('accounts', function($scope,$http,$document) {
    $scope.accounts = [];
    $scope.error = null;
    $scope.editMode = 'new';
    var newAccountTemplate = {
        title:'',
        type:'',
        initialBalance:'',
        note:'',
        group:''
    };


    $scope.newAccount = newAccountTemplate;

    $scope.createNewAccount = function() {
        $scope.newAccount = JSON.parse(JSON.stringify(newAccountTemplate));
        $scope.editMode = 'new';
        $('#addAccount').modal('show');
    };

    // @param acc The Account object to edit
    $scope.editAccount = function(acc) {
      $scope.newAccount = JSON.parse(JSON.stringify(acc));
      $scope.editMode = 'edit';
      $('#addAccount').modal('show');  
    };

	$scope.calcTotal = function() {
		var total = 0;
		$scope.accounts.forEach(e=>total+=e.balance);
		return total;
	};

    $scope.refresh = function() {
        $http.get('/api/accounts')
        .success(function(data) {
            $scope.accounts = data;
        })
        .error(function(err) {
            $scope.error = "Could not load account information from server";
        });
    };

    // Used for both save and create accounts
    $scope.saveAccount = function() {
      
      if ($scope.newAccount._id === undefined) {
          // Create New. 
          // submission
          $http.post('/api/accounts', JSON.stringify($scope.newAccount))
          .success(function(result) {
             if (result.success) {
                 $('#addAccount').modal('hide');
                 $scope.refresh();
             } else {
                 $scope.error = "Could not create the account: " + result.error.msg;
             }
          })
          .error(function(err) {
             $scope.error = "Could not create the account: " + err;
          });
      } else {
          // Edit old
          $http.put('/api/accounts/' + $scope.newAccount._id, $scope.newAccount)
          .then(function(result) {
              if (result.data.success) {
                  $('#addAccount').modal('hide');
                  $scope.refresh();
              } else {
                  $scope.error = "Could not save the account: " + result.data.error.msg;
              }
          }, function onError(err) {
              $scope.error = "Could not save the account: " + err.toString();
          });
      }
      
    };

    $scope.refresh();
});
