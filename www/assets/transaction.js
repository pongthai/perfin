"use strict";

var app = angular.module('perfin');

app.filter('longDate', function() {

	return function(input) {
		let d= new Date(input);
		const days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];	
		const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

		return days[d.getDay()] + ' ' + d.getDate() + ' ' +  months[d.getMonth()] + ' ' + (d.getYear() + 1900);
	}
});

app.filter('time', function() {

	return function(input) {
		let d = new Date(input);

		let pad = function(num,pos) {
			let snum = String(num);
			let pad = '';
			for (let i=0; i<pos - snum.length; i++) pad += '0';
			return pad + snum;
		}

		return pad(d.getHours(),2) + ':' + pad(d.getMinutes(),2);
	}
});


app.controller('transaction', function($rootScope,$scope,$http,$q,$routeParams,listAccounts, $timeout) {
    $scope.transactions = [];
    $scope.error = null;
	$scope.alertMsg = null; 
	$scope.alertClass = 'alert-success';
    $scope.accountList = [];
    $scope.cats = {};
	$scope.prevdate = '';

	$scope.$watch('alertMsg', function(newval, oldval) {
		if (newval) {
			$timeout(()=>$scope.alertMsg='', 5000);	
		}
	});

    listAccounts().then(function(accs) {
        $scope.accountList = accs;
    });
    
    if (typeof($routeParams.a) !== 'undefined') {
        $scope.account = $routeParams.a;
    }

	$scope.getAccountObject = function(acc_id) {
		return $scope.accountList.find(acc => acc._id === acc_id);
	};

    // @return promise
    $scope.getCatInfo = function(catId) {
        return $q(function(resolve,reject) {
            if ($scope.cats[catId]!== undefined) {
                resolve($scope.cats[catId]);
            } else {
                $http.get('/api/cats/'+catId)
                .success(function(data) {
                    var cat = data.result;
                    $scope.cats[cat._id] = cat;
                    resolve($scope.cats[cat._id]);
                })
                .error(function(errmsg) {
                    reject(errmsg);
                });
            }
        });
    };


    $scope.refresh = function() {
        if ($scope.account !== '') {
            $http.get('/api/accounts/'+$scope.account+'/trans')
            .success(function(data) {
                if (data.success) {
                    $scope.transactions = data.result;
                    // load icons and colors
                    $scope.transactions.forEach(function(tr) {        
                        (function(tr) {
                            $scope.getCatInfo(tr.category).then(function(cat){
                                tr.icon = cat.icon;
                                tr.color = cat.color;
                            });
                        })(tr);
                    });

                    // Make sure other pages see the same default account
                    $rootScope.account = $scope.account;
                }
            })
            .error(function(err) {
                $scope.alertMsg = "Could not load transaction information from server";
				$scope.alertClass = 'alert-danger';
                console.log(err);
            });
        }
    };

    $scope.deleteTran = function(tr)
    {
        $http.delete('/api/trans/' + tr._id)
        .then(function() {
			$scope.alertMsg = 'Deleted.';
			$scope.alertClass = 'alert-success';
			$scope.refresh();	
        })
        .catch(function(err) {
			$scope.alertMsg = 'Error deleting the transaction';
			$scope.alertClass = 'alert-danger';
           	console.log(err); 
        });  
    };

    $scope.refresh();
});
