var app = angular.module('perfin');

// catProvider Service
// usage: catProvider()
// @return Promise that resolves to array of categories with slug.
app.factory('catProvider', ['$q','$http', function($q,$http) {
   return function() {
        var cats = [];
        var buildCatSlug = function(cat)
        {
            if (cat.parent !== undefined && cat.parent !== null) {
                var parentData;
                for (var i=0; i<cats.length; i++) {
                    if (cats[i]._id === cat.parent) {
                        parentData = cats[i];
                        break;
                    }
                }
                return buildCatSlug(parentData) + ' > ' + cat._id;
            } else {
                return cat._id;
            }
        };

        return $http.get('/api/cats')
            .then(function(result) {
                if (result.data.success) {
                    cats = result.data.result;
                    // build a slug
                    cats.forEach(function(cat) { 
                        cat.slug = buildCatSlug(cat);
                    });
                    // Sort so that it is sorted by slug
                    cats.sort(function(a,b) { if (a.slug < b.slug) return -1; else if (a.slug > b.slug) return 1; else return 0; });
                    return $q.resolve(cats);
                } else {
                    return $q.reject(result.data.error);
                }
            },
            function onError(err) {
                return $q.reject(err);   
            });
    };
}]);

app.controller('cat', ['$scope','$http', 'catProvider', function($scope,$http,catProvider) {
    $scope.cats = [];
    $scope.alert = '';
    $scope.reassignTo = '';
    $scope.alertType = ''; // '' == hidden
    $scope.toBeDeleted = null;

    $scope.refresh = function() {
        catProvider().then(function(cats) {
            $scope.cats = cats;
        });
    };

    $scope.refresh();

    $scope.dismissAlert = function() {
        $scope.alertType='';
    };

    $scope.delcat= function(cat) {
        $scope.toBeDeleted = cat;
        $scope.reassignCats = JSON.parse(JSON.stringify($scope.cats));
        var index = -1;
        $scope.reassignCats.forEach(function(e,i) { if (e._id === cat._id) index = i; });
        $scope.reassignCats.splice(index, 1); // exclude the deletion target category from reassignable list.

        $('#reassignDlg').modal('show');
    };

    $scope.performDelete = function() 
    {
        if ($scope.toBeDeleted !== null) {

           $http.delete('/api/cats/' + $scope.toBeDeleted._id + '?reassign=' + $scope.reassignTo._id)
           .then(function(res) {
               if (res.data.success) {
                    $scope.alert = "Category Deleted.";
                    $scope.alertType = 'alert alert-success';
                    $scope.refresh();
               } else {
                     $scope.alert = "Could not delete the category.";
                    $scope.alertType = 'alert alert-danger';
               }
           },
           function onError(res) {
                 $scope.alert = "Could not delete the category.";
                    $scope.alertType = 'alert alert-danger';
           });  

            $scope.toBeDeleted = null;
        }
        $('#reassignDlg').modal('hide');
       
    };
}]);