'use strict';

var mongo = require('mongodb');
var Promise = require('bluebird');

mongo.connect('mongodb://localhost/perfin')
.then(function(db) {
	db.collection('trans').find({},{date:1}).toArray()
	.then(function(trans) {
		var promises = [];

		trans.forEach(function(tran,i) {
			if (!tran.date.match(/\.\d{3}$/)) {
				console.log("updating transaction " + tran._id);
				promises.push(db.collection('trans').updateOne({_id:tran._id},
					{$set:{date:tran.date + '.000'}}));
			} else {
				console.log("skip " + tran._id + " as already patched.");
			}
		});

		return Promise.all(promises);
	})
	.then(function() {
		db.close();
	});
});
