"use strict";

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var uuid = require('uuid');
var process = require('process');
var mongo = require('mongodb');
var dbstr = "mongodb://localhost/perfin";
var Promise = require('bluebird');
var format = require('date-format');
var Log = require('log');
var fs = require('fs');
var accounts = {};
var transactions = [];
var db = null;
var DATE_FORMAT = 'yyyy-MM-ddThh:mm:ss.SSS';
var port = 3000;

app.use(express.static('../www'));
app.use(bodyParser.json());

// external database support 
if (process.env.npm_package_config_dbstr) dbstr = process.env.npm_package_config_dbstr;
if (process.env.npm_package_config_port) port = process.env.npm_package_config_port;

var loglevel = 'debug';
var logfile = 'perfin.log';

if (fs.existsSync(logfile)) {
	var fnames = logfile.split(/\./);
	var timestamp = format('_yyyy-MM-dd_hhmmss', new Date());
	fs.createReadStream(logfile).pipe(fs.createWriteStream(fnames[0]+timestamp + '.' + fnames[1]));
}

var log = new Log(loglevel, fs.createWriteStream(logfile));

// Indicates that all responses are application/json formatted
app.use(function(req,res,next) {
	res.set('Content-type','application/json');
	log.debug("client connected from %s, retrieving %s", req.ip, req.originalUrl);
	log.debug("   request data is = '%s'", JSON.stringify(req.body));
	next();
});

// list all accounts
// @params g (optional) Filter only for the certain account group
app.get(['/api/accounts','/api/accounts/:accountId'], function(req, res) {
	var query = {};

	if (req.params.accountId !== undefined) query._id = req.params.accountId;
	if (req.params.g !== undefined) query.group = req.params.g;

	db.collection("accounts").find(query).toArray().then(
		function(accounts) {
			res.send(JSON.stringify(accounts));
		},
		function(err) {
			res.send(JSON.stringify(err));	
		});
});


// create an account
/*
	@body { 
		title: string, // * account title
		type: string // * CASH|SAVINGS|CREDIT|OTHER 
		initialBalance: number, //  initial balance = 0.0
		group: string, // account group = ''
		note: string, // account notes = ''
	}
*/ 
app.post('/api/accounts', function(req, res) {
	var body = req.body;		
 	var acc = {};

	acc._id = 'A' + uuid.v4().replace(/\-.*$/, ''); 


	try {
		// validate parameters 
		fillData(body, acc, 'title');
		fillData(body, acc, 'type');
		fillData(body, acc, 'initialBalance', 0.0, 'number');
		fillData(body, acc, 'group', '');
		fillData(body, acc, 'note', '');

		acc.balance = Number(acc.initialBalance);

		if (['CASH','SAVINGS','CREDIT','OTHERS'].indexOf(acc.type)<0) {
			throw {code: '403', msg: "invalid account type"};
		}

		// valiate initial balance
		if (Number(acc.initialBalance)===NaN) {
			throw {code: '413', msg: 'Invalid account initial balance'};
		}

		// Write to database
		db.collection("accounts").insertOne(acc).then(
			function() {
				res.send(JSON.stringify({
					success:true, 
					result:{
						accid:acc._id
					},
				}
				));
			}, 
			function(err) {
				res.send(JSON.stringify({success:false, msg:err}));
			});
	} catch(err) {
		log.error(JSON.stringify(err));
		res.send(JSON.stringify({
			success: false,
			errmsg: 'Check missing fields',
			error: err 
		}));
	}
});


/**
	Edit an account identified by the given account id

	@param account	The account ID
	@body  @see account object definition
*/
app.put('/api/accounts/:account', function(req, res) {
	var account = req.params.account;
	if (account === undefined) {
		res.status(400).send('Bad request - account id parameter is required');
		return;
	}

	db.collection('accounts').find({_id:req.params.account}).limit(1).next()
	.then(function(acc) {
		var newacc = req.body;
		
		// if initial balance changed, recalculate account balance
		if (newacc.initialBalance !== acc.initialBalance) {
			recalculateBalance(acc._id);
		}

		for (var k in acc) {
			if (newacc[k] !== undefined) {
				acc[k] = newacc[k];
			}
		}
		
		return db.collection('accounts').update({_id:req.params.account}, acc);
	})
	.then(function(insres) {
		res.send(JSON.stringify({success:true}));	
	})
	.catch(function(err) {
		log.error(err);
		res.status(500).send('Internal Server Error: ' + err);
	});
});

// delete an account
app.delete('/api/accounts/:account', function(req, res) {
	var account = req.params.account;
	if (account === undefined) {
		res.status(400).send('Bad request: account id parameter is required');
		return;
	}

	db.collection('accounts').deleteOne({_id:req.params.account})
	.then(function(dr) {
		// Remove the account
		if (dr.result.n >= 1) {
			// Remove all related transactions
			db.collection('trans').deleteMany({$or:[{account: req.params.account},{account: req.params.toAccount}]});

			// Send response without waiting for transaction deletion to complete.
			res.send(JSON.stringify({success:true}));
		} else {
			res.status(404).send(JSON.stringify({success:false, error:{code:'404', msg:'Not found'}, result:dr }));
		}
	})
	.catch(function(err) {
		log.error(err);
		res.status(500).send('Internal Server Error: ' + err);
	});;
});

/** TRANSACTION

	@body {
		payee: string, // * payee
		type: string, // * EXPENSE | INCOME | TRANSFER | ADJUST   // Adjust is for when balance is incorrect
		amount: number, // * amount of transaction, required for all types except AJUST when balance is given.
		balance: number, // Optional, only valid when type is ADJUST, otherwise ignored. Server will calculate amount automatically given the new balance
		category: string, // category = ''
		date: number, // Transaction date and time in format YYYY-mm-ddTHH:mm  = today()
		toAccount: string, // destination account id, required for TRANSFER
		note: string, // transaction note
	}

	@note If the type is ADJUST and both balance and amount were given and  both are not agree, the amount shall take total priority.
	@note the account field is ignored if supplied. the :account parameter is the only place to specify account.
*/
app.post('/api/accounts/:account/trans', doPutPostTrans);

app.put('/api/accounts/:account/trans/:trans_id', doPutPostTrans);

function doPutPostTrans(req, res) {
	var trans = {};		

	try {
		log.debug("trans called with method = " + req.route.stack[0].method);

		if (req.route.methods.post) 
			trans._id = 'T' + uuid.v4().replace(/\-.*$/,'');
		else 
			trans._id = req.params.trans_id;

		// the account should not be given, but if it is, error should be given back to the caller if it does not agree with parameter
		if (req.body.account!==undefined && req.body.account !== req.params.account) {
			throw {code:"401", msg:"Account field in the body and in the url not agree. Drop the one in the body and retry."};
		}

		fillData(req.params, trans, 'account');
		fillData(req.body, trans, 'payee');
		fillData(req.body, trans, 'type');  
		fillData(req.body, trans, 'date', new Date().toISOString());
		fillData(req.body, trans, 'category', '');
		fillData(req.body, trans, 'toAccount', null);

		if (req.body.amount !== undefined) { trans.amount = req.body.amount; }
		if (req.body.balance !== undefined) { trans.balance = req.body.balance; }

		
		// Check if type is valid
		if (['EXPENSE','INCOME','TRANSFER','ADJUST'].indexOf(trans.type) < 0) {
			throw {code:"406", msg:"Invalid Transaction Type"};
		}

		// Check for a valid date
		if (isNaN(new Date(trans.date))) {
			throw {code:"407", msg:"Invalid Transaction Date"};
		} else {
			// reformat date to match server formatting
			trans.date = new Date(trans.date).toISOString();
		}

		// If type is ADJUST, validation will pass when either balance or amount is given and is number. 
		if (trans.type==="ADJUST" && trans.amount === undefined) {
			if (trans.balance === undefined || Number(trans.balance)===NaN) {
				throw {code:"420", msg:"either amount or balance must be given when type is ADJUST"};
			}	
		} else if (trans.amount === undefined || Number(trans.amount)===NaN) { 
		// when it is other types, Check for valid amount
			throw {code:"412", msg:"Invalid Amount - Non existence or Non numeric"};
		}

		// Check if account exists
		db.collection("accounts").find({_id:trans.account}).next()
		.then(function(acc) {
			if (acc!==null) {
				// fetch the account balance and bring in to calculate amount in case of ADJUST with balance given
				if (trans.type === 'ADJUST' && trans.balance !== undefined) {
					trans.amount = Number(trans.balance) - acc.balance; // negative allowed
					delete trans.balance; // balance must be transient so that the calculateBalance can do its job.
				}
				return Promise.resolve();
			} else {
				return Promise.reject({code:"405", msg:"Account not existed"});	
			}
		})
		.then(function() {
			// Make sure destination account exists in case of transfer
			if (trans.type === 'TRANSFER') {
				db.collection("accounts").find({_id:trans.toAccount}).count()
				.then(function(count) {
					if (count > 0) {
						return Promise.resolve();
					} else {
						return Promise.reject({code:"406", 
						msg:"Destination Account ID is required in case of TRANSFER typed transaction."});
					}
				});
			} else {
				return Promise.resolve();	
			}
		})
		.then(function() {
			// Check if category exists, if not blank.
			// The blank category is allowed.
			if (trans.category !== "") {
				return db.collection("cats").find({_id:trans.category}).count()
				.then(function(count) {
					if (count > 0) {
						return Promise.resolve();
					} else {
						return Promise.reject({code:"413",
						msg:"Invalid Category"});
					}
				});
			} else {
				return Promise.resolve();
			}
		})
		.then(function() {
			// Save the transaction
			return db.collection("trans").updateOne({_id:trans._id}, trans, {upsert:true});
		})
		.then(function() {
			// Update the account balance for the affected account(s)
			return recalculateBalance(trans.account);
		})
		.then(function() {
			if (trans.type === 'TRANSFER') {
				return recalculateBalance(trans.toAccount);
			} else {
				return Promise.resolve();
			}
		})
		.then(function() {
			// Update category usage count, which is used when sorting cat by popularity
			return db.collection("cats").updateOne({_id: trans.category}, {$inc:{usages:1}});
		})
		.then(function() {
			// Save to Payee information / template for later autosuggestion uses.
			return db.collection("payees").update({_id:trans.payee}, {
					amount: trans.amount,
					account: trans.account,
					toAccount: trans.toAccount,
					type: trans.type,
					category: trans.category,
					lastUsed: new Date().getTime()
				}, {upsert: true} );
		})
		.then(function() {
			// Send the result
			res.send(JSON.stringify({success:true, result:{transactionId:trans._id}}));
		})
		.catch(function(err) {
			res.status(500).send(JSON.stringify({success:false, errmsg:'' , error:err}));
		});
	} catch (err) {
		if (err instanceof Error) log.error('client error: ' + err);
		else log.error('client error: '  + JSON.stringify(err)); 

		res.status(500).send(JSON.stringify({success:false, error:err}));		
	}
}

/**
	Query transactions in an account

	/api/accounts/:accountId/trans?from=dateStart&to=dateEnd

	accountId = the account to include in the result (also include transfer-in).
	dateStart = date to start the filter (Optional)
	dateEnd = date to end the filter (Optional)


	@note	mind the 'account' field in the response for the TRANSFER type. If account is not your targetted account then the transfer is INWARD.
			In that case the balance for the target account is precalculated and stored in the dstBalance field instead of balance field. The balance
			field belongs to the owner of the transaction (the OUTWARD account). 
*/
app.get('/api/accounts/:account/trans', function(req, res) {
	try {
		if (req.params['account'] === undefined)
		{
			throw {code:'404', msg:'Missing Account Id'};
		} 

		db.collection('trans').find({$or:[{account:req.params.account},{toAccount:req.params.account}]}).sort({date:-1}).toArray()
		.then(function(trans) {
			// filter the date range 
			var result = trans.filter(function(tr) {
				if (req.query.from !==undefined && Date.parse(req.query.from) < Date.parse(tr.date)) return false;
				if (req.query.to !== undefined && Date.parse(req.query.to) > Date.parse(tr.date)) return false;

				// format the date and save it in formattedDate field for UI display.
				var d = new Date(tr.date);
				tr.formattedDate = d.getDate() + '/' + (d.getMonth() + 1) + '/' + (d.getYear() + 1900);
				return true;
			});

			res.send(JSON.stringify({success:true, result:result}));
		})
		.catch(function(err) {
			res.status(500).send(JSON.stringify({success:false, error:{code:"400", msg:'Internal Server Error: ' + err}}));	
		});	
	} catch (err) {
		log.error(err);
		res.status(400).send(JSON.stringify({success:false, error:err}));
	}
});

app.get('/api/trans/:transId', function(req, res) {
	if (req.params.transId === undefined) {
		res.status(400).send(JSON.stringify({success:false, error:{code:'401', msg:'Missing Transaction ID'}}));
		return;
	}

	db.collection('trans').find({_id:req.params.transId}).limit(1).next()
	.then(function(tran) {
		res.send(JSON.stringify({success:true, result:tran}));	
	}, function(err) {
		res.status(500).send(JSON.stringify({success:false, error:err}));
	});
});

app.delete('/api/trans/:transId', function(req, res) {
	try {
		if (req.params.transId === undefined) throw {code:'409', msg:'No transaction id given for delete.'};
	 
	    // Delete the transaction
		db.collection('trans').findOneAndDelete({_id:req.params.transId})
		.then(function(result) {
			// Recalculate the balances for the accounts affected by the deleted transaction 
			recalculateBalance(result.value.account);
			if (result.value.type === 'TRANSFER')
				recalculateBalance(result.value.toAccount);

			res.send(JSON.stringify({success:true}));
		})
		.catch(err => {
			res.send(JSON.stringify({success:false, error: err}));
		});
	} catch (err) {
		log.error(err);
		res.send(JSON.stringify({success:false, error:err}));
	}
});

/**
	Create a new category or update the existing one with new data
	@param _id string of unique id for the certain category
	@body { 
		parent: string, // * null if on root. parent _id if not.
		description: string, // description note
		type: string, // EXPENSE, INCOME, TRANSFER, ADJUST
		color: string, // color code = random
		icon: string, // glyphicon-x
	}
*/
app.put('/api/cats/:_id', function(req, res) {
	var cat = {};	

	try {
		fillData(req.params, cat, '_id');
		fillData(req.body, cat, 'parent', null);
		fillData(req.body, cat, 'description', '');
		fillData(req.body, cat, 'color', '#cccc66');
		fillData(req.body, cat, 'icon', 'glyphicon glyphicon-tag');
		fillData(req.body, cat, 'type', 'EXPENSE');
		cat.usages = 0; // Indicates popularity


		// Cat type check, we do not 
		if (['EXPENSE','INCOME','TRANSFER','ADJUST'].indexOf(cat.type) < 0) throw {error:'501',msg:'Invalid Category Type'};

		// make sure parent category exists

		new Promise(function(resolve, reject) {
			if (cat.parent !== null) {
				db.collection('cats').find({_id:cat.parent}).count()
				.then(function(c) {
					if (c > 0) {
						resolve();
					} else {
						reject({code:'410', msg:'Parent category not exist'});
					}
				});
			} else {
				resolve();
			}
		})
		.then(function() {
			db.collection('cats').updateOne({_id:cat._id}, cat, {upsert:true})
			.then(function(result) {
				res.send({success:true, result:result});
			})
			.catch(function(err) {
				res.status(500).send(JSON.stringify({success:false, error:err}));	
			});
		})
		.catch(function(err) {
			res.status(500).send(JSON.stringify({success:false, error:err}));	
		});		
	} catch(err) {
		log.error(err);
		res.status(500).send(JSON.stringify({success:false, error:err}));
	}
});

/**
 *
 */
app.get('/api/cats/:catid', function(req, res) {
		
	db.collection('cats').find({_id:req.params.catid}).limit(1).next()
	.then(function(cat) {
		res.send(JSON.stringify({success:true, result:cat}));
	})
	.catch(function(err) {
		log.error(err);
		res.status(500).send(JSON.stringify({success:false, error:err}));
	});
});

/**
	Retrieve all categories
	
	@query	p	optional string	parent category
	@query  q	optional string partial match (starts with) 
	@query  l   optional number limit number of results returned

	@return { 
		success: true,
		result: [ 
			{
				_id: string, // category id (with - separated path)
				color: string,
				icon: string
			}, ...
		]
	}
*/
app.get('/api/cats', function(req, res) {
	var query = {};
	if (req.query.p !== undefined) {
		if (req.query.p === 'null') {
			query.parent = null;
		} else {
			query.parent = req.query.p;
		}
	}

	if (req.query.q !== undefined) {
		query._id = {$regex:req.query.q};
	}

	var cursor = db.collection('cats').find(query).sort({usages:1});
	
	if (req.query.l !== undefined) {
		cursor = cursor.limit(Number(req.query.l));
	}

	cursor.toArray()
	.then(function(cats) {
		res.send(JSON.stringify({success:true, result:cats}));
	})
	.catch(function(err) {
		log.error(err);
		res.status(500).send(JSON.stringify({success:false, error:err}));
	});
});

/**
	@param	catid	category id of which to be deleted
	@query  reassign string  the id of the category transactions to be reassigned to
						default to blank category.
 */
app.delete('/api/cats/:catid', function(req, res) {

	var catid = req.params.catid;
	var reassign = req.query.reassign;

	if (!catid) { 
		res.status(400).send(JSON.stringify({success:false, error:{code:411, msg:'Require category ID'}}));
		return;
	}

	if (reassign === undefined) reassign = "";

	// Make sure the target category does not have children. Otherwise, deletion is not allowed.
	db.collection('cats').find({parent:catid}).count()
	.then(function(count) {
		if (count > 0) {
			return Promise.reject({code:'415', msg:'Could not delete the parent category. Delete all children first.'});
		} else {
			return Promise.resolve();
		}
	})
	.then(function() {
		// Delete the category 
		db.collection('cats').remove({_id:catid});

		// Remove category or reassign category from entries that are using the deleted cat
		db.collection('trans').update({category:req.params.catid}, {$set:{category:reassign}});

		res.send(JSON.stringify({success:true}));
	})
	.catch(function(err) {
		res.status(500).send(JSON.stringify(err));
	});
});
/**
	PAYEE
	
	GET /api/payees

	@query q	Query string for autocompletion
	@query l	Number to limit results

	Retrieve list of payees for autocompletion purpose. The result 
	is sorted upon most recently used.
*/

app.get('/api/payees', function(req, res) {
	if (req.query.q === undefined) req.query.q = '';
	if (req.query.l === undefined) req.query.l = 20;

	var query = {_id:{$regex:'^'+req.query.q}};

	// Allow for limiting account
	if (req.query.a !== undefined) query.account=req.query.a;

	db.collection('payees').find(query).limit(Number(req.query.l)).sort({lastUsed:-1}).toArray()
	.then(function(payees) {
		res.send(JSON.stringify({success:true, result:payees}));
	})
	.catch(function(err) {
		res.status(500).send(JSON.stringify({success:false, error:err}));
	});
});

/** Utiltiy function that helps copying field data to destination object.
	It also has an ability to use default value upon missing of original 
	data.
	
	@param input An object that will be the source of data
	@param output An object that will be the destination 
	@param field A field to copy
	@param defaultValue A default value to use in case of input field absence
	@param type Either 'string' or 'number'. Default is string. 
*/
function fillData(input, output, field, defaultValue, type)
{
	var out = "";

	if (input[field] === undefined) { 
		if (defaultValue === undefined) {
			throw {code:'403',msg:'Field ' + field + ' has no value.', field: field};
		}

		out = defaultValue;
	} else { 
		out = input[field];
	}

	switch (type) {
	case "number": output[field] = Number(out); break;
	case "string": output[field] = out; break;
	case "boolean": output[field] = out ? true : false; 
	default: output[field] = out;
	}
}

// Recalculate balances by traversing through all transactions and update transaction 
// and account balance to reflect what really is the correct ones. 
// 
// @param acc_id The Account ID for an account to recalculate balances 
// @return Promise then(function(finalBalance){});
function recalculateBalance(acc_id)
{
	log.debug("Recalculating balance for " + acc_id);

	try {
		if (acc_id === undefined || typeof(acc_id) !== 'string') 
			return Promise.reject({code:'403', msg:'No Account ID Given to recalculate balance'});	

		// filter the account in two ways:
		// 1. Direct : Transactions those are done on this account
		// 2. Indirect : Transactions those are of TRANSFER type which has this as a destination account

		db.collection("trans").find({$or:[{account:acc_id},{toAccount:acc_id}]}).sort({date:1}).toArray()
		.then(function(trans) {

			// gets the initial account balance
			db.collection("accounts").find({_id:acc_id},{initialBalance:1}).limit(1).next()
			.then(function(acc) {

				log.debug("The account " + acc_id + " has initial balance = " + acc.initialBalance);

				var balance = Number(acc.initialBalance);
				trans.forEach(function(tr,i) {
					switch (tr.type) {
						case "TRANSFER": 
							if (tr.account === acc_id) { balance -= Number(tr.amount); }
							else if (tr.toAccount === acc_id) { 
									balance += Number(tr.amount); 
								}
							break;
						case "INCOME": 
							balance += Number(tr.amount);
							break;
						case "EXPENSE":
							balance -= Number(tr.amount);
							break;
						case "ADJUST": 
							if (tr.balance !== undefined && !isNaN(Number(tr.balance))) {
								let new_amount = tr.balance - balance;
								balance = tr.balance;
								// The rule for ADJUST is, if the transaction has 'balance' set, it is the information to rely on
								// even though there is an 'amount' being set. Hence, if the amount is not null and it has a different
								// value if calculated backward. This could happen for the case when other transaction was updated or removed, 
								// The 'amount' for this transaction should be corrected accordingly unless there was no amount set before.
								balance = tr.balance;
								if (tr.amount && tr.amount !== new_amount) {
									db.collection('trans').updateOne({_id:tr._id},{$set:{amount:new_amount}});	
								}
							} else {
								balance += Number(tr.amount);
							}
							break;
						default:
							throw {code:"408",msg:"Invalid transaction type detected on transaction ID = " + tr._id};
					}

					if (tr.type === 'TRANSFER' && tr.toAccount === acc_id) {
						// In the case of transfer INWARD, the balance is set to 'dstBalance' as two accounts share the same transaction entry.
						// However, the outward account is an actual owner of the transaction. A single transaction is good when we need to edit
						// or delete the transaction.
						tr.dstBalance = balance;
						// Now update the affected transactions, without write concern
						db.collection('trans').updateOne({_id:tr._id}, {$set:{dstBalance:tr.dstBalance}});
					} else {
						// In normal cases, the balance set for this transaction should be straightforwardly set.
						tr.balance = balance;
						// Now update the affected transactions, without write concern
						db.collection('trans').updateOne({_id:tr._id}, {$set:{balance:tr.balance}});
					}
				});	

				// Update the account, again, without write concern
				db.collection('accounts').updateOne({_id:acc_id}, {$set:{balance:balance}});

				return Promise.resolve(balance);
			})
			.catch(function(err) {
				return Promise.reject({success:false, error:{code:"400", msng:'Internal Server Error: ' + err}});	
			});	
		})
		.catch(function(err) {
			return Promise.reject({success:false, error:{code:"400", msg:err}});
		});

	} catch (err) {
		log.error(err);
		return Promise.reject({success:false, error:err});
	}
}

// Main Procedure
log.debug("trying to connect to %s", dbstr);

mongo.connect(dbstr)
.then(function(_db) {
	db = _db;
	log.info("Database connected.");
	app.listen(port, '0.0.0.0'); // force ipv4 bindings
	log.info("Listening on " + port);
}, 
function(err) {
	log.error("Could not connect to database. Terminating.");
});






