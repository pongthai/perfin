"use strict";

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var uuid = require('uuid');

var accounts = {};
var transactions = [];

app.use(express.static('../www'));
app.use(bodyParser.json());

app.use(function(req,res,next) {
	res.set('Content-type','application/json');
	next();
});

// list all accounts
app.get('/api/accounts', function(req, res) {
	var result = [];
	for (var k in accounts) { result.push(acccounts[k]); }
	res.send(JSON.stringify(result));	
});

// create an account
/*
	@body { 
		title: string, // * account title
		type: string // * CASH|SAVINGS|CREDIT|OTHER 
		initialBalance: number, //  initial balance = 0.0
		group: string, // account group = ''
		note: string, // account notes = ''
	}
*/ 
app.post('/api/accounts', function(req, res) {
	var body = req.body;		
 	var acc = {};

	acc._id = 'A' + uuid.v4().replace(/\-.*$/, ''); 


	try {
		fillData(body, acc, 'title');
		fillData(body, acc, 'type');
		fillData(body, acc, 'initialBalance', 0.0);
		fillData(body, acc, 'group', '');
		fillData(body, acc, 'note', '');

		if (['CASH','SAVINGS','CREDIT','OTHERS'].indexOf(acc.type)<0) {
			throw {code: '403', msg: "invalid account type"};
		}

		// Check for duplication
		if (accounts[acc._id] !== undefined) throw {code:"404", msg:'Duplicated account ID'};
		
		// Save to central store
		accounts[acc._id] = acc;
	
		// Provide output
		res.send(JSON.stringify({
				success:true, 
				result:{
					accid:acc._id
				},
			}
			));
	} catch(err) {
		console.log("error:" + JSON.stringify(err));
		res.send(JSON.stringify({
			success: false,
			error: err
		}));
	}
});

// edit an account
app.put('/api/accounts/:accountid', function(req, res) {
});

// delete an account
app.delete('/api/accounts/:account', function(req, res) {
});

/** TRANSACTION

	@body {
		account: string, // * account id
		payee: string, // * payee
		type: string, // * EXPENSE | INCOME | TRANSFER | ADJUST   // Adjust is for when balance is incorrect
		amount: number, // * amount of transaction
		date: string, // date of transaction = today()
		category: string, // category = 'Uncategorized'
		toAccount: string, // destination account id, required for TRANSFER
		note: string, // transaction note
	}
*/
app.post('/api/accounts/:account/trans', function(req, res) {
	var trans = {};		

	try {
		trans._id = 'T' + uuid.v4().replace(/\-.*$/,'');
		fillData(req.params, trans, 'account');
		fillData(req.body, trans, 'payee');
		fillData(req.body, trans, 'type');  
		fillData(req.body, trans, 'amount');
		fillData(req.body, trans, 'date', new Date().toString());
		fillData(req.body, trans, 'category', 'Uncategorized');
		fillData(req.body, trans, 'toAccount', null);


		// Check if type is valid
		if (['EXPENSE','INCOME','TRANSFER','ADJUST'].indexOf(trans.type) < 0) {
			throw {code:"406", msg:"Invalid Transaction Type"};
		}

		// Check for a valid date
		if (Date.parse(trans.date) === NaN) { 
			throw {code:"407", msg:"Invalid Transaction Date"};
		}

		// Check if account exists
		if (accounts[trans.account] === undefined) {
			throw {code:"405",msg:"Non-existing account given."};
		}

		// Make sure destination account exists in case of transfer
		if (trans.type === 'TRANSFER' && trans.toAccount === undefined) { 
			throw {code:"406", msg:"Destination Account ID is required in case of TRANSFER typed transaction."};
		}

		if (accounts[trans.account] === undefined) {
			throw {code:"405",msg:"Non-existing account given.(destination)"};
		}

		transactions.push(trans);	

		res.send(JSON.stringify({success:true, result:{transactionId:trans._id}}));
	} catch (err) {
		res.send(JSON.stringify({success:false, error:err}));		
	}
});

/**
	Query transactions in an account

	/api/accounts/:accountId/trans?from=dateStart&to=dateEnd

	accountId = the account to include in the result (also include transfer-in).
	dateStart = date to start the filter (Optional)
	dateEnd = date to end the filter (Optional)

*/
app.get('/api/accounts/:account/trans', function(req, res) {
	try {
		if (req.params['account'] === undefined)
		{
			throw {code:'404', msg:'Missing Account Id'};
		} 

		// filter the account in two ways:
		// 1. Direct : Transactions those are done on this account
		// 2. Indirect : Transactions those are of TRANSFER type which has this as a destination account
		var result = transactions.filter(function(tr) {
			if (tr.type === 'TRANSFER' && tr.toAccount === req.paramse.account) return true;
			return tr.account === req.params.account;
		});

		// Sort the transaction dates
		result.sort(function(a,b) { 
			var dateA = Date.parse(a.date);
			var dateB = Date.parse(b.date);
			if (dateA > dateB) return 1;
			else if (dateA < dateB) return -1;
			return 0;
		});

		// Add the balance calculation to the result
		var account = accounts[req.params.account];
		if (account === undefined) throw { code:"405", msg:"Non-Existing Account" }; 
	
		var balance = account.initialBalance;

		result.forEach(function(tr,i) {
			switch (tr.type) {
				case "TRANSFER": 
					if (tr.account === account._id) { balance -= tr.amount; }
					else if (tr.toAccount === account._id) { balance += tr.amount; }
					break;
				case "INCOME": 
					balance += tr.amount;
					break;
				case "EXPENSE":
					balance -= tr.amount;
					break;
				case "ADJUST": 
					tr.variation = tr.amount - balance;
					balance = tr.amount;
					break;
				default:
					throw {code:"408",msg:"Invalid transaction type detected on transaction ID = " + tr._id};
			}
			tr.balance = balance;
		});	

		// filter the date range 
		result = result.filter(function(tr) {
			if (req.query.from !==undefined && Date.parse(req.query.from) < Date.parse(tr.date)) return false;
			if (req.query.to !== undefined && Date.parse(req.query.to) > Date.parse(tr.date)) return false;
			return true;
		});

		res.send(JSON.stringify({success:true, result:result}));

	} catch (err) {
		console.log(err);
		res.send(JSON.stringify({success:false, error:err}));
	}
});

/**
	Create a new category or update the existing one with new data
	@param _id string of unique id for the certain category
	@body { 
		parent: string, // * null if on root. parent _id if not.
		description: string, // description note
		color: string, // color code = random
		icon: string, // glyphicon-x
	}
*/
app.put('/api/cats/:_id', function(req, res) {
	var cat = {};	

	try {
		fillData(cat, req.params, '_id');
		fillData(cat, req.body, 'parent', null);
		fillData(cat, req.body, 'description', '');
		fillData(cat, req.body, 'color', '#cccc66');
		fillData(cat, req.body, 'icon', 'glyphicon glyphicon-tag');

		db.collection('categories').update({_id:cat._id}, cat, {upsert:1})
		.then(function(result) {
			res.send({success:true});
		})
		.catch(function(err) {
			res.status(500).send(JSON.stringify({success:false, error:err});	
		});
			
	} catch(err) {
		console.log(err);
		res.status(500).send(JSON.stringify({success:false, error:err});
	}
});

/**
*/
app.get('/api/cats/:catid', function(req, res) {
		
	db.collection('cats').find({_id:req.params.catid}).limit(1).next()
	.then(function(cat) {
		res.send(JSON.stringify({success:true, result:cat}));
	})
	.catch(function(err) {
		console.log(err);
		res.status(500).send(JSON.stringify({success:false, error:err});
	});
});

/**
	Retrieve all categories
	
	@query	p	optional string	parent category
	@query  q	optional string partial match (starts with) 

	@return { 
		success: true,
		result: [ 
			{
				_id: string, // category id (with - separated path)
				color: string,
				icon: string
			}, ...
		]
	}
*/
app.get('/api/cats', function(req, res) {
	var query = {};
	if (req.query.p !== undefined) {
		if (query.p=== 'null') {
			query.parent = null;
		} else {
			query.parent = req.queries.p;
		}
	}

	if (req.query.q !== undefined) {
		query._id = {$regex:req.query.q};
	}
	
	db.collection('cats').find(query).toArray()
	.then(function(cats) {
		res.send(JSON.stringify({success:true, result:cats}));
	})
	.catch(function(err) {
		console.log(err);
		res.status(500).send(JSON.stringify({success:false, error:err});
	});
});

/** Utiltiy function that helps copying field data to destination object.
	It also has an ability to use default value upon missing of original 
	data.
	
	@param input An object that will be the source of data
	@param output An object that will be the destination 
	@param field A field to copy
	@param defaultValue A default value to use in case of input field absence
*/
function fillData(input, output, field, defaultValue)
{
	if (input[field] === undefined) { 
		if (defaultValue === undefined) {
			throw {code:'403',msg:'Field ' + field + ' has no value.', field: field};
		}

		output[field] = defaultValue;
	} else { 
		output[field] = input[field];
	}
}

app.listen(3000);

